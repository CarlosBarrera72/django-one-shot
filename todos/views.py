from django.shortcuts import render, get_object_or_404,redirect
from todos.models import TodoList,TodoItem
from todos.forms import TodoListForm,TodoItemForm

def show_TodoList(request):
    todolists = TodoList.objects.all()
    context = {
        "todoList" : todolists
    }
    return render(request, "todo/todolist.html",context)

def todo_list_detail(request, id):
    todolist = get_object_or_404(TodoList, id = id)
    todoitems = todolist.items.all()
    context = {
        "todolist" : todolist,
        "todoitems" : todoitems,
    }
    return render(request, "todo/todolistdetail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect('todo_list_detail', id = todo_list.id)
    else:
        form = TodoListForm()

    context = {
        'form' : form,
    }

    return render(request,"todo/create_todolist.html", context)

def todo_list_update(request,id):
    todolist = get_object_or_404(TodoList, id = id)

    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id = id)
    else:
        form = TodoListForm(instance=todolist)

    context = {
        'form': form,
        'todolist': todolist,
    }
    return render(request,'todo/update_todolist.html', context)

def todo_list_delete(request, id):
    todolist = get_object_or_404(TodoList, id = id)

    if request.method == 'POST':
        todolist.delete()
        return redirect('todo_list_list')

    context ={
        'todolist' : todolist,
    }

    return render(request, 'todo/delete_todolist.html', context)

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save()
            return redirect('todo_list_detail', id = todo_item.list.id)
    else:
        form = TodoItemForm()

    todolists = TodoList.objects.all()

    context ={
        "form":form,
        "todolists":todolists

    }

    return render(request, 'todo/create_todoitem.html', context)

def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id = id)

    if request.method == "POST":
        form = TodoItemForm(request.POST, instance = todo_item)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id = todo_item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)

    context = {
        'form' : form,
        'todo_item' : todo_item,
        'todolists' : TodoList.objects.all()
    }

    return render(request, 'todo/update_todoitem.html', context)
